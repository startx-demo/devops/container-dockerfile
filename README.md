# Creating your container

## Getting started

### 1. Create a fork of this project.

- Create a gitlab account if not already done. Go to [Gitlab sign-in page](https://gitlab.com/users/sign_in).
- Login into your gitlab account
- Go to this [repository homepage](https://gitlab.com/startx-demo/devops/container-dockerfile) and click on the [fork button](https://gitlab.com/startx-demo/devops/container-dockerfile/-/forks/new).
- Be sure that the fork you're creating is public.
- read the [vim tutorial](https://www.tutorialspoint.com/vim/vim_getting_familiar.htm) if you need to now more on how to edit files on remote linux server

### 2. clone your project

When you are under you own copy of the repository, click on the clone button, and copy the http url to download your repository

```bash
git clone https://gitlab.com/<your_username>/container-dockerfile.git (! don''t forget the .git at the end)
cd container-dockerfile
```

### 3. Configure your environment

#### 3.1. Create devel branch

Create your new development branch named **devel**

```bash
git checkout -b devel
```

#### 3.2. Install podman command

Install the podman command. It will install all the dependencies required to run container using **runc**.

```bash
sudo yum install -y podman
```

### 4. Create your bash application

#### 4.1. Create your bash file

Create a file named **myapp.sh** and change permission to allow group and user to execute this file.

```bash
touch myapp.sh
chown ug+x myapp.sh
```

Edit you script 

```bash
vi myapp.sh
```

Add the following elements, in this order :

- A [bash shebang](https://linuxize.com/post/bash-shebang) on the first line
- Declare a variable named **APP_NAME** with a value of your choice (eg: My Best Script)
- Create a case structure and listen to the **$1** variable (second parameter of our script when executed).
  If the value is **welcome**, you must display a message saying **Welcome to** and the name of your application
- Record your changes

```bash
#!/bin/bash
APP_NAME="My App Name"

case $1 in
    welcome)    echo "Welcome to $APP_NAME";;
esac
```

Test your script

```terminal
[user@localhost]$ ./myapp.sh welcome
Welcome to My App Name
```

Commit your change in the devel branch with

```bash
git add myapp.sh
git commit -m "adding my first version of myapp.sh"
```

#### 4.2. Add the random feature

Edit your script

```bash
vi myapp.sh
```

Add a new case when user use the action **random**. The action to perform is to display the content of
the RANDOM special environment variable.

```bash
case $1 in
    welcome)    echo "Welcome to $APP_NAME";;
    random)     echo $RANDOM;;
esac
```

Test your changes

```terminal
[user@localhost]$ ./myapp.sh random
3213
```

Commit your change in the devel branch with

```bash
git add myapp.sh
git commit -m "adding random option"
```

#### 4.3. Add the date feature

Edit your script

```bash
vi myapp.sh
```

Add a new case when user use the action **date**. The action to perform is to execute the date command.
If an argument is passed to your date subcommand, then you must pass it to the date command.

```bash
case $1 in
    welcome)    echo "Welcome to $APP_NAME";;
    random)     echo $RANDOM;;
    date)       date $2;;
esac
```

Test your changes

```terminal
[user@localhost]$ ./myapp.sh date 
Fri 22 Apr 13:23:35 CEST 2022
[user@localhost]$ ./myapp.sh date +%Y
2022
```

Commit your change in the devel branch with

```bash
git add myapp.sh
git commit -m "adding date option"
```

#### 4.4. Add the user feature

Edit your script

```bash
vi myapp.sh
```

Add a new case when user use the action **user**. The action to perform is to display the username and user id information.
You could use the **whoami** command to get the username, and the **id** command to get the user context. 
Decide how to display both of theses information into the same output.

```bash
case $1 in
    welcome)    echo "Welcome to $APP_NAME";;
    random)     echo $RANDOM;;
    date)       date $2;;
    user)                   
                echo "user: $(whoami)"
                echo "id:   $(id)"
    ;;
esac
```

Test your changes

```terminal
[user@localhost]$ ./myapp.sh user 
user: cl
id:   uid=1000(cl) gid=1000(cl) groups=1000(cl)...
```

Commit your change in the devel branch with

```bash
git add myapp.sh
git commit -m "adding user option"
```

#### 4.5. Add the manual

Edit your script

```bash
vi myapp.sh
```

Add a new case for all other action except the one defined previously. The action to perform is to display
a man page explaining how to use this command. Here is an example of this documentation.

```txt
# Usage

[user@localhost]$ myapp ACTION [OPTIONS]

# Availables actions

- welcome              Get a welcome message
- random               Get a random number (0 to 32767)
- date [format]        Get the current date (use format %Y syntax to change representation of the date)
- user                 Return information about the current user
```

```bash
case $1 in
    welcome)    echo "Welcome to $APP_NAME";;
    random)     echo $RANDOM;;
    date)       date $2;;
    user)                   
                echo "user: $(whoami)"
                echo "id:   $(id)"
    ;;
    *)
    echo "Welcome to $APP_NAME"
    cat <<EOF
---------------

# Usage

[user@localhost]$ myapp ACTION [OPTIONS]

# Availables actions

- welcome              Get a welcome message
- random               Get a random number (0 to 32767)
- date [format]        Get the current date (use format %Y syntax to change representation of the date)
- user                 Return information about the current user

EOF
esac
```

Test your changes

```terminal
[user@localhost]$ ./myapp.sh --help 
# Usage
...
```

Remove the execution right to your file

```bash
chmod ug-x myapp.sh
```

Commit your change in the devel branch with

```bash
git add myapp.sh
git commit -m "adding manpage option"
```

Merge your work into the main branch

```bash
git checkout main
git merge devel
git checkout devel
```

### 5. Create your Dockerfile file

Create a file named **Dockerfile** with the following content

- one base image using ***quay.io/startx/fedora***
- one environment variable named MY_APP with your prefered value
- this image run with the uid 1001 by default. Change the execution user to root
- copy your myapp.sh script into the /app/ directory of the container. Rename it myapp (without extention)
- Change permission to allow execution of this script for user and group
- Change ownership of this script to user 1001 and group 0
- Change back the executing user 1001
- Add an entrypoint with your script
- Add an optionnal argument (command) with the value help

```dockerfile
FROM quay.io/startx/fedora
ENV MY_APP="MyApplicationName"
USER root
COPY myapp.sh /app/myapp
RUN chown -R 1001:0 /app/myapp && \
    chmod -R ug+x /app/myapp
USER 1001
ENTRYPOINT [ "/app/myapp" ]
CMD [ "help" ]
```

Build your container

```terminal
[user@localhost]$ podman build . -t myapp:test
...
```

Test your container

```terminal
[user@localhost]$ podman run myapp:test
# Usage
...
[user@localhost]$ podman run myapp:test date
Fri 22 Apr 13:23:35 CEST 2022
...
```

If everything is fine, create an imutable tag name

```terminal
[user@localhost]$ podman build . -t myapp:1.0.0
...
```

Commit your change in the main branch

```bash
git add list.yml
git commit -m "adding my first version of list.yml
```
